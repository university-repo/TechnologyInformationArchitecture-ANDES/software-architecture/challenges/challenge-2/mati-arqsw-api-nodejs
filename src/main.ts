import { ServerConfiguration } from "./infrastructure/server/server.configuration";
import { sendErrorResponse } from "./adapters/filters/error.handler";
import taxRouter from './adapters/routes/tax.router';

const server = new ServerConfiguration();
const app = server.app;
const fullApiPath = server.fullApiPath;

app.use(fullApiPath, taxRouter);
app.use(sendErrorResponse);

export default app;
