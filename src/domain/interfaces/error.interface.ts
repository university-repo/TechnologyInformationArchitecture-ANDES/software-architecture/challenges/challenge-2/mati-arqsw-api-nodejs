export interface IAPIErrorHandler {
    uuid: string;
    StatusCode: number;
    Description: string;
    TimeStamp: string;
}
